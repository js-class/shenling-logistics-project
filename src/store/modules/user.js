// 两根线
// 同步：组件 commit -> mutations -> state
// 异步： 组件 dispatch -> actions -> commit -> mutations -> state
import { login } from '@/api/user'
import { setToken, getToken, removeToken } from '@/utils/auth'
// import { Message } from 'element-ui'

export default {
  // 开启命名空间
  namespaced: true,
  // 放数据的地方
  state: () => ({
    token: getToken() || ''

  }),
  // 相当于组件中计算属性
  getters: {},
  // 这个是唯一修改state中数据地方
  mutations: {
    setToken(state, payload) {
      state.token = payload
      setToken(payload)
    }
  },
  // 写异步的ajax的地方
  actions: {
    async login(context, payload) {
      const res = await login(payload)
      console.log(res)
      setToken(payload)
      context.commit('setToken', payload)
    },
    logout(context) {
      context.commit('setToken', '') // 修改本地token为空
      removeToken() // 清除本存储的token
      // context.commit('userInfo', '') // 清除用户信息
    }

  }
}
