import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
    meta: { title: '登录' }
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '工作台', icon: 'dashboard' }
    }]
  },

  {
    path: '/shuju',
    component: Layout,
    redirect: '/src/views/shuju/index.vue',
    name: 'ShuJu',
    meta: { title: '基础数据管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'shuju',
        name: 'ShuJu',
        component: () => import('@/views/shuju/index.vue'),
        meta: { title: '机构管理' }
      },
      {
        path: 'fanwei',
        name: 'FanWei',
        component: () => import('@/views/shuju/fanwei.vue'),
        meta: { title: '机构作业范围' }
      },
      {
        path: 'yunfei',
        name: 'YunFei',
        component: () => import('@/views/shuju/yunfei.vue'),
        meta: { title: '运费管理' }
      }
    ]
  },

  {
    path: '/car',
    component: Layout,
    meta: { title: '车辆管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'index',
        name: 'CarPage',
        component: () => import('@/views/car/index.vue'),
        meta: { title: '车辆管理' }
      },
      {
        path: 'carlist',
        name: 'CarList',
        component: () => import('@/views/car/carlist.vue'),
        meta: { title: '车辆列表' }
      },
      {
        path: 'huiche',
        name: 'HuiChe',
        component: () => import('@/views/car/huiche.vue'),
        meta: { title: '回车登记' }
      }
    ]
  },

  {
    path: '/staff',
    component: Layout,
    name: 'StaffPage',
    meta: {
      title: '员工管理',
      icon: 'nested'
    },
    children: [
      {
        path: 'staff',
        name: 'StaffPage',
        component: () => import('@/views/staff/index.vue'),
        meta: { title: '快递员管理' }
      },
      {
        path: 'driver',
        name: 'DriverPage',
        component: () => import('@/views/staff/driver.vue'),
        meta: { title: '司机管理' }
      },
      {
        path: 'paiban',
        name: 'PaiBan',
        component: () => import('@/views/staff/paiban.vue'),
        meta: { title: '排班管理' }
      }
    ]
  },

  {
    path: '/order',
    component: Layout,
    meta: { title: '业务管理管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'order',
        name: 'OrderPage',
        component: () => import('@/views/order/order.vue'),
        meta: { title: '订单管理' }
      },
      {
        path: 'yundan',
        name: 'YunDan',
        component: () => import('@/views/order/yundan.vue'),
        meta: { title: '运单管理' }
      }
    ]
  },

  {
    path: '/diaodu',
    component: Layout,
    meta: { title: '调度管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'index',
        name: 'DiaoDu',
        component: () => import('@/views/diaodu/index.vue'),
        meta: { title: '运输任务管理' }
      },
      {
        path: 'line',
        name: 'LinePage',
        component: () => import('@/views/diaodu/line.vue'),
        meta: { title: '线路管理' }
      },
      {
        path: 'cancel',
        name: 'CancelPage',
        component: () => import('@/views/diaodu/cancel.vue'),
        meta: { title: '取消作业管理' }
      },
      {
        path: 'paijian',
        name: 'PaiJian',
        component: () => import('@/views/diaodu/paijian.vue'),
        meta: { title: '派件作业管理' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
