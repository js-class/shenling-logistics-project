// https://slwl-api.itheima.net   基地址

import axios from 'axios'
import store from '@/store'
// import { Message } from 'element-ui'

const service = axios.create({
  timeout: 10000,
  baseURL: 'https://slwl-api.itheima.net'
})

// 添加请求拦截器
service.interceptors.request.use(function(config) {
  const token = store.getters.token
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }
  // 在发送请求之前做些什么
  return config
}, function(error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
service.interceptors.response.use(function(response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  return response // .data返回的东西少一点
}, function(error) {
  console.log(error)
  // if (error.response.status.toString().startsWith('4')) {
  //   Message.error(error.response.data.message)
  // }
  // 报错提醒
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  return Promise.reject(error)
})

export default service
