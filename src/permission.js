import router from '@/router'
import store from '@/store'
const whiteList = ['/login', '/404'] // 白名单
import defaultSettings from '@/settings'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// 前置路由守卫
router.beforeEach((to, from, next) => {
  NProgress.start()
  const token = store.getters.token
  if (token) {
    if (to.path === '/login') {
      next('/')
    } else {
      next()
    }
  } else {
    if (whiteList.includes(to.path)) {
      next()
    } else {
      next('/login')
    }
  }
  NProgress.done()
})

// 后置路由守卫
// 如果当前路由规则没有title就用默认title，有就 -title
router.afterEach((to) => {
  NProgress.done()
  if (to.meta.title) {
    document.title = defaultSettings.title + '-' + to.meta.title
  } else {
    document.title = defaultSettings.title
  }
})
